package main.org.hillel.java.introduction.lessonten;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class HomeworkTen {

    public static void main(String[] args) throws IOException {

        Country greatBritain = new Country("Великобритания");
        Country uSA = new Country ("США");
        Country georgia = new Country("Грузия");
        Country italy = new Country("Италия");
        Country russia = new Country("Россия");
        Country france = new Country("Франция");

        Author authorWithoutABook = new Author("Александр Пушкин", 1799, russia);
        Author authorOne = new Author("Терри Пратчетт", 1948, greatBritain);
        Author authorTwo = new Author("Дениз Вудз", 1958,uSA);
        Author authorThree = new Author("Кэтрин Стокетт", 1969,uSA);
        Author authorFour = new Author("Борис Акунин", 1956, georgia);
        Author authorFive = new Author("Джон Вердон", 1942, uSA);
        Author authorSix = new Author("Донато Карризи", 1973 , italy);
        Author authorSeven = new Author("Роберт Гэлбрейт", 1965, greatBritain);
        Author authorEight = new Author("Стейс Крамер", 1996, russia);
        Author authorNine = new Author("Пола Хокинс", 1972, greatBritain);
        Author authorTen = new Author("Валери Перрен", 1967, france);

        Author[] authors = {authorWithoutABook, authorOne, authorTwo, authorThree, authorFour, authorFive,
                            authorSix, authorSeven, authorEight, authorNine, authorTen};

        Year twentyZeroTwo = new Year(2002);
        Year twentyZeroNine = new Year(2009);
        Year twentyTen  = new Year(2010);
        Year twentyEleven  = new Year(2011);
        Year twentyFifteen  = new Year(2015);
        Year twentyEighteen  = new Year(2018);

        Year[] years = {twentyZeroTwo, twentyZeroNine, twentyTen, twentyEleven, twentyFifteen, twentyEighteen};

        Book bookOne = new Book("Ночная Стража", twentyZeroTwo, authorOne,"978-5-699-52239-2");
        Book bookTwo = new Book("Ночной поезд в Инсбрук", twentyZeroTwo, authorTwo,"5-94278-795-6");
        Book bookThree = new Book("Прислуга", twentyZeroNine, authorThree, "978-3-64-105940-8");
        Book bookFour = new Book("Весь мир театр", twentyZeroNine, authorFour, "978-5-42-502301-8");
        Book bookFive = new Book("Загадай число", twentyTen, authorFive,"978-8-58-041038-9");
        Book bookSix = new Book("Потерянные девушки Рима", twentyEleven, authorSix, "978-8-77-137126-0");
        Book bookSeven = new Book("Зажмурься покрепче", twentyEleven, authorFive, "978-8-58-041094-5");
        Book bookEight = new Book("На службе зла", twentyFifteen, authorSeven,"978-3-64-118858-0");
        Book bookNine = new Book("50 дней до моего самоубийства", twentyFifteen, authorEight, "978-5-17-088673-9");
        Book bookTen = new Book("Девушка в поезде", twentyFifteen, authorNine, "978-9-89-880055-8");
        Book bookEleven = new Book("Планета Вода", twentyFifteen, authorFour, "978-5-45-776516-0");
        Book bookTwelve = new Book("Поменяй воду цветам", twentyEighteen, authorTen, "978-5-04-105396-3");

        Book[] books = {bookOne, bookTwo, bookThree, bookFour, bookFive, bookSix, bookSeven,
                                            bookEight, bookNine, bookTen, bookEleven, bookTwelve};

        HomeworkTen homeworkTen = new HomeworkTen();

        homeworkTen.printAllData(books);

        int searchType = homeworkTen.chooseSearchType();
        if(searchType == 1){
            homeworkTen.searchBookByYearFromUser(books, years);
        }
        if(searchType == 2){
            homeworkTen.searchBookByAuthor(books, authors);
        }
    }

    private void printBook(Book book) {
        System.out.println("Book \"" + book.getName() + "\" by " + book.getAuthor().getName() +
                ", published in " + book.getYear().getYear() + ", ISBN: " +
                book.getIsbnNumber() + ", seller: " + Book.getSeller() + ".");
    }

    private void printAllData(Book[] books){

        for(Book bookObject: books){
            printBook(bookObject);
        }
    }

    private void searchBookByYearFromUser(Book[] books, Year[] years)  {

        Year yearFromUser = getYearFromUser(years);
        boolean bookFound = false;
        for (Book book : books) {
            if (yearFromUser == book.getYear()) {
                printBook(book);
                bookFound = true;
            }
        }
        if(!bookFound) {
            System.out.println("Search completed. No books found.");
        }
    }

    private void searchBookByAuthor(Book[] books, Author[] authors) throws IOException {

        Author authorFromUser = getAuthorFromUser(authors);
        boolean bookByAuthorFound = false;
        for ( Book book : books ) {
            if (authorFromUser == book.getAuthor()) {
                printBook(book);
                bookByAuthorFound = true;
            }
        }
        if (!bookByAuthorFound) {
            System.out.println("Search completed. No books found.");
        }
    }

    private Year getYearFromUser(Year[] years)  {

        System.out.println("Please, enter the year.");
        while (true) {
            Scanner scanner = new Scanner(System.in);
            if (scanner.hasNextInt()) {
                int intFromUser = scanner.nextInt();
                if (intFromUser >= 868 && intFromUser <= 2021) {
                    for (Year year : years){
                        if(intFromUser == year.getYear()){
                            scanner.close();
                            return year;
                        }
                    }
                    System.out.println("No books found. Please, try another year.");
                } if(intFromUser > 2021) {
                    System.out.println("This year is yet to come. Please, enter valid year.");
                }
                if(intFromUser < 868){
                    System.out.println("No books were printed those days. Please, enter valid year.");
                }
            } else {
                System.out.println("The symbols you entered are not numbers. Please, enter valid year.");
            }
        }
    }

    private Author getAuthorFromUser(Author[] authors) throws IOException {

        System.out.println("Please, enter the author.");
        while (true) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String stringFromUser = reader.readLine();
            for (Author author : authors){
                if(stringFromUser.equalsIgnoreCase(author.getName())){
                    reader.close();
                    System.out.println(author.getName() + " was born in "
                            + author.getYearOfBirth() + " in " + author.getCountry().getName() + ".");
                    return author;
                }
            }
            System.out.println("No books found. Please, check the spelling or try another author.");
        }
    }

    private int chooseSearchType(){
        System.out.println("To search books by year - enter 1, by author - enter 2.");
        while (true) {
            Scanner scanner = new Scanner(System.in);
            if (scanner.hasNextInt()) {
                int userChoice = scanner.nextInt();
                if (userChoice == 1 || userChoice == 2) {
                    return userChoice;
                }
            } else {
                System.out.println("Please, enter 1 to search books by year or 2 to search by author");
            }
        }
    }
}
