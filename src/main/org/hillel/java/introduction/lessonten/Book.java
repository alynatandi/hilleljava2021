package main.org.hillel.java.introduction.lessonten;

public class Book {

    private String name;
    private Year year;
    private Author author;
    private String isbnNumber;
    private static String seller = "Litress";


    public Book(String name, Year year, Author author, String isbnNumber) {
        this.name = name;
        this.year = year;
        this.author = author;
        this.isbnNumber = isbnNumber;
    }

    public String getName() {
        return name;
    }

    public Year getYear() {
        return year;
    }

    public Author getAuthor() {
        return author;
    }

    public String getIsbnNumber() {
        return isbnNumber;
    }

    public static String getSeller() {
        return seller;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", year=" + year +
                ", author=" + author +
                ", isbnNumber='" + isbnNumber + '\'' +
                '}';
    }
}
