package main.org.hillel.java.introduction.lessonten;

public class Author {
    private String name;
    private int yearOfBirth;
    private Country country;

    public Author(String name, int yearOfBirth, Country country) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public Country getCountry() {
        return country;
    }

    @Override
    public String toString() {
        return "Author{" +
                "name='" + name + '\'' +
                ", yearOfBirth=" + yearOfBirth +
                ", country='" + country + '\'' +
                '}';
    }
}
