package main.org.hillel.java.introduction.dontunderstand;

public class SwordsMan extends Man {

    private static String weapon  = "sword";


    public SwordsMan(String name) {
        super(name);
    }

    @Override
    public void fight() {
        System.out.println("I'll slaughter you with a " + SwordsMan.weapon + ".");
    }
}
