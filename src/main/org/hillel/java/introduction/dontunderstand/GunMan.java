package main.org.hillel.java.introduction.dontunderstand;


public class GunMan extends Man {

    private static String weapon = "Gun";


    public GunMan(String name) {
        super(name);
    }

    @Override
    public void fight() {
        System.out.println("I'll shoot you with a " + GunMan.weapon + ".");
    }
}
