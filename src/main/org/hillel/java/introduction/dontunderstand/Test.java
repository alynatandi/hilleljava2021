package main.org.hillel.java.introduction.dontunderstand;

public class Test {

    public static void main(String[] args) {
        GunMan gunMan = new GunMan("Gunman Tom");
        SwordsMan swordsMan = new SwordsMan("Swordsman Jerry");
        Man[] men = {gunMan, swordsMan};

        for (Man man : men) {
            man.fight();
        }

    }
}

