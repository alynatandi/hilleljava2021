package main.org.hillel.java.introduction.lessonnine;

import main.org.hillel.java.introduction.lessonnine.Book;

import java.io.IOException;
import java.util.Scanner;

public class HomeworkNineVersionTwo {

    public static void main(String[] args) {

        Year twentyZerot = new Year(2002);
        Year twentyZeroTwo = new Year(2002);
        Year twentyZeroNine = new Year(2002);
        Year twentyTen  = new Year(2010);
        Year twentyEleven  = new Year(2011);
        Year twentyFifteen  = new Year(2015);
        Year twentyEghteen  = new Year(2018);

        Book bookOne = new Book("Ночная Стража", twentyZeroTwo.year, "978-5-699-52239-2");
        Book bookTwo = new Book("Ночной поезд в Инсбрук", twentyZeroTwo.year, "5-94278-795-6");
        Book bookThree = new Book("Прислуга", twentyZeroNine.year, "978-3-64-105940-8");
        Book bookFour = new Book("Весь мир театр", twentyZeroNine.year, "978-5-42-502301-8");
        Book bookFive = new Book("Загадай число", twentyTen.year, "978-8-58-041038-9");
        Book bookSix = new Book("Потерянные девушки Рима", twentyEleven.year, "978-8-77-137126-0");
        Book bookSeven = new Book("Зажмурься покрепче", twentyEleven.year, "978-8-58-041094-5");
        Book bookEight = new Book("На службе зла", twentyFifteen.year, "978-3-64-118858-0");
        Book bookNine = new Book("50 дней до моего самоубийства", twentyFifteen.year, "978-5-17-088673-9");
        Book bookTen = new Book("Девушка в поезде", twentyFifteen.year, "978-9-89-880055-8");
        Book bookEleven = new Book("Планета Вода", twentyFifteen.year, "978-5-45-776516-0");
        Book bookTwelve = new Book("Поменяй воду цветам", twentyEghteen.year, "978-5-04-105396-3");

        Book[] books = { bookOne, bookTwo, bookThree, bookFour, bookFive, bookSix,
                bookSeven, bookEight, bookNine, bookTen, bookEleven, bookTwelve};//

        printAllData(books);
        searchBookByYearFromUser(books);
    }

    public static void printAllData(Book[] books){

        for(Book bookObject: books){
            System.out.println("Book \"" + bookObject.name + "\", published in " + bookObject.year + ", ISBN: " +
                    bookObject.isbnNumber + ", seller: " + Book.seller + ".");
        }
    }


    public static void searchBookByYearFromUser(Book[] books) {
        int yearFromUser = getYearFromUser();
        boolean bookFound = printBooksByYearFromUser(books, yearFromUser);
        if(!bookFound) {
            System.out.println("Search completed. No books found.");
        }
    }

    private static boolean printBooksByYearFromUser(Book[] books, int yearFromUser) {

        boolean bookFound = false;
        for (Book book : books) {
            if (yearFromUser == book.getYear()) {
                System.out.println("\"" + book.getName() + "\" " + ", ISBN: " +
                        book.getIsbnNumber() + ", seller: " + Book.getSeller() + ".");
                bookFound = true;
            }
        }
        return bookFound;
    }


    private static int getYearFromUser() {
        System.out.println("Please, enter the year.");
        while (true) {
            Scanner scanner = new Scanner(System.in);
            if (scanner.hasNextInt()) {
                String stringFromUser = scanner.nextLine();
                if (stringFromUser.length() == 4) {
                    int intFromUser = Integer.parseInt(stringFromUser);
                    scanner.close();
                    return intFromUser;
                } else {
                    System.out.println("Please, enter valid year.");
                }
            } else {
                System.out.println("Please, enter valid year.");
            }
        }
    }
}
