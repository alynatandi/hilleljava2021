package main.org.hillel.java.introduction.lessonnine;

public class Year {


    public int year;

    public Year(int year) {
        this.year = year;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "Year{" +
                "year=" + year +
                '}';
    }
}
