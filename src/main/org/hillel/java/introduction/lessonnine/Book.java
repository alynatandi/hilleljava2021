package main.org.hillel.java.introduction.lessonnine;

import main.org.hillel.java.introduction.lessonnine.Year;

public class Book {
    public String name;
    public int year;
    public String isbnNumber;
    public static String seller = "Litress";



    public Book(String name, int year, String isbnNumber) {
        this.name = name;
        this.year = year;
        this.isbnNumber = isbnNumber;
    }

    public String getName() {
        return name;
    }

    public int getYear() {
        return year;
    }

    public String getIsbnNumber() {
        return isbnNumber;
    }

    public static String getSeller() {
        return seller;
    }
}
