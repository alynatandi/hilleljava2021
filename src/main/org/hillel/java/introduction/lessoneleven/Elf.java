package main.org.hillel.java.introduction.lessoneleven;

public class Elf extends Warrior {

    public Elf(String name, int age, String sex, int strength) {
        super(name, age, sex, strength);
        this.setRace("Elf");
    }

    public void run(){
        System.out.println("I'm already here!");
    }

    @Override
    public void tellAboutYourself() {
        super.tellAboutYourself();
        System.out.println("Lok-Tar Ogar!");
        System.out.println();
    }

    public void eat(){
        System.out.println("Breath is a life-giving force!");
    }
}