package main.org.hillel.java.introduction.lessoneleven;

public class ElfArcher extends Elf {

    public ElfArcher(String name, int age, String sex, int strength) {
        super(name, age, sex, strength);
        this.setWeapon(new Weapon("bow", 100));
    }

    @Override
    public void run() {
        System.out.println("Archer doesn't need to run. Arrow flies fast.");
    }
}
