package main.org.hillel.java.introduction.lessoneleven;

public class HomeworkEleven {

    public static void main(String[] args) {

        Warrior[] warriors = {
                new Elf("Silverman", 100, "male", 30),
                new Elf("Yogi", 15, "male", 10),
                new ElfArcher("Beretta", 25, "female", 5),
                new ElfArcher("Yorwett", 35, "female", 5),
                new ElfArcher("Legolas", 30, "male", 25),
                new ElfSwordsman("Eredin", 20, "male", 15),
                new ElfSwordsman("Avallakh", 150, "male", 60),
                new ElfSwordsman("Yarpen", 60, "male", 30),
                new ElfSwordsman("Zigrin", 200, "male", 120),
                new ElfSwordsman("Shammgod", 50, "male", 20),
                new Dwarf("Cheery Littlebottom", 45, "female", 30, 100)
        };

        HomeworkEleven homeworkEleven = new HomeworkEleven();

        for (Warrior warrior: warriors){
            warrior.tellAboutYourself();
            System.out.println();
        }

        for (Warrior warrior: warriors){
            warrior.attackWithDamage();
        }
        System.out.println();

        homeworkEleven.totalAttack(warriors);
        System.out.println();

//       Получается, таки может быть объект абстрактного класа?
        Warrior cheeryLittlebottom = warriors[10];
        cheeryLittlebottom.attackWithDamage();
        System.out.println();

        cheeryLittlebottom.eat();
        System.out.println();

        cheeryLittlebottom.eat();
        System.out.println();

        cheeryLittlebottom.attackWithDamage();
    }

    private void totalAttack(Warrior[] warriors){
        int totalAttackDamage = 0;
        for (Warrior warrior: warriors){
            totalAttackDamage += warrior.attackWithDamage();
        }
        System.out.println("Total damage is " + totalAttackDamage);
        System.out.println();
    }
}
