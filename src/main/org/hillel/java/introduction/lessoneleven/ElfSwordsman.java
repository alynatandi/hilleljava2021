package main.org.hillel.java.introduction.lessoneleven;

public class ElfSwordsman extends Elf {

    public ElfSwordsman(String name, int age, String sex, int strength) {
        super(name, age, sex, strength);
        this.setWeapon(new Weapon("sword", 150));
    }




}
