package main.org.hillel.java.introduction.lessoneleven;

public class Dwarf extends Warrior {

    private int bellyful;
    private static final int SATIETYLOSSPERATTACK = 20;

        public Dwarf(String name, int age, String sex, int strength, int bellyful) {
        super(name, age, sex, strength);
        this.bellyful = bellyful;
        this.setWeapon(new Weapon("axe", 200));
        this.setRace("Dwarf");
    }

    public int getBellyful() {
        return bellyful;
    }
    public void setBellyful(int bellyful) {
        this.bellyful = bellyful;
    }

    @Override
    public void tellAboutYourself() {
        super.tellAboutYourself();
        int satiety = getBellyful();
        if(satiety < 80){
            System.out.println("I'm hungry!");
        }
        System.out.println("Heil to Odin!");
        System.out.println();
    }

    @Override
    public void eat() {
       int satiety = getBellyful();
       if(satiety >= 100){
           System.out.println("I'm full. Thank you.");
       } else{
           setBellyful(100);
           System.out.println("Now I'm ready to fight!");
       }
    }

    @Override
    public void run() {
        System.out.println("Waddle, waddle, waddle.");
    }

   @Override
    public int attackWithDamage() {
        int satiety = getBellyful();
        if(satiety < 80) {
            System.out.println("Hungry Dwarf is a bad warrior.");
            return 0;
        }
        setBellyful((satiety-SATIETYLOSSPERATTACK));
        return super.attackWithDamage();
    }
}
