package main.org.hillel.java.introduction.lessoneleven;

public abstract class Warrior<weapon> {

    private String name;
    private int age;
    private String sex;
    private int strength;
    private String race;
    private Weapon weapon;

    public Warrior(String name, int age, String sex, int strength) {
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.strength = strength;
        this.weapon = null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

        public void tellAboutYourself(){
        System.out.println("I'm " + this.race + " " + this.name + ".");
        if(this.weapon == null){
            System.out.println("I'm unarmed.");
        }else{
            System.out.println("I'm armed with a " + this.weapon.getName() + " and its damage is " + this.weapon.getDamage());
        }
    }

    private int strikeWithDamage(){
        int totalDamage = 0;
        if(this.weapon == null){
            System.out.println(this.race + " " + this.name + " strikes with bare hands and deals " + this.strength + " damage.");
            totalDamage += this.strength;
        } else {
            System.out.println(this.race + " " + this.name + " strikes with a " + this.weapon.getName() + " and deals " + (this.strength + this.weapon.getDamage()) + " damage.");
            totalDamage += this.strength + this.weapon.getDamage();
        }
        return totalDamage;
    }

    public abstract void eat();
    public abstract void run();

    public int attackWithDamage(){
        run();
        return strikeWithDamage();
    }

    public void defend(){
        System.out.println(this.name + " defends.");
    }
}

